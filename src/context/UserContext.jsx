import { createContext, useContext, useState } from "react"

export const UserContext = createContext(null)

export const useUserContext = () => {
    return useContext(UserContext)
}

function UserProvider({children}) {

    const [username, setUsername] = useState("")

    return (
        <UserContext.Provider value={[username, setUsername]}>{ children }</UserContext.Provider>
    )

}

export default UserProvider