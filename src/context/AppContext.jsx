import CountProvider from "./CountContext"
import UserProvider from "./UserContext"

function AppContext({ children }) {

    return (
        <UserProvider>
            <CountProvider>
                {children}
            </CountProvider>
        </UserProvider>
    )
}

export default AppContext