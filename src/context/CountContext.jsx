import { createContext, useContext, useState } from "react"

export const CountContext = createContext()

export const useCountContext = () => {
    return useContext(CountContext)
}

function CountProvider({ children }) {

    const [count, setCount] = useState(0)

    return (
        <CountContext.Provider value={[count, setCount]}>
            {children}
        </CountContext.Provider>
    )
}

export default CountProvider