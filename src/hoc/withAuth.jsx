import { useUserContext } from "../context/UserContext"
import { Navigate } from "react-router-dom"

const withAuth = Component => props => {

    const [username, ] = useUserContext()

    if (username)
        return <Component {...props} />
    else
        return <Navigate to='/'/>
}

export default withAuth