import { useEffect } from "react"

function Guitar(props) {

    useEffect(() => {
        console.log(props);
    }, []) // should run once (list of dependencies is empty)

    return (
        <>
            { props.guitar.brand && props.guitar.model && <h3>{props.guitar.brand} - {props.guitar.model}</h3>}
            { props.guitar.numberOfStrings && <p>Strings: {props.guitar.numberOfStrings}</p>}
        </>
    )
}

export default Guitar