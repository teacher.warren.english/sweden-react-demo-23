import './Navbar.css'
import { NavLink } from "react-router-dom"

function Navbar() {

    return (
        <nav>
            <ul>
                <li><NavLink to='/' >Coffees</NavLink></li>
                <li><NavLink to='/guitars' >Guitars</NavLink></li>
            </ul>
        </nav>
    )
}

export default Navbar