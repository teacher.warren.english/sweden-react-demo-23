import { useEffect, useState } from "react"
import { API_URL } from '../utils'
import Coffee from "./Coffee"

function CoffeeList() {

    const [coffees, setCoffees] = useState([])

    useEffect(() => {
        fetch(API_URL)
            .then(response => response.json())
            .then(json => {
                console.log(json)
                // assign it to local state
                setCoffees(json)
            })
            .catch(error => console.error(error.message))
    }, [])

    return (
        <>
            {coffees && coffees.map(coffee => <Coffee key={coffee.id} currentCoffee={coffee} />)}
        </>
    )
}

export default CoffeeList