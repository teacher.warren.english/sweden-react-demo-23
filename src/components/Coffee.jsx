import { useState } from "react"

function Coffee({ currentCoffee }) {

    const [flag, setFlag] = useState(false)

    const handleCoffeeClicked = () => {
        setFlag(!flag)
    }

    return (
        <>
            <h3 onClick={handleCoffeeClicked}>{ currentCoffee.description }</h3>
            <p>{ currentCoffee.price } {flag && `⭐`}</p>
        </>
    )
}

export default Coffee