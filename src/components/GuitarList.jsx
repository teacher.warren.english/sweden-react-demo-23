import Guitar from './Guitar'

function GuitarList() {

    const myGuitars = [
        {
            model: "Stratocaster",
            brand: "Fender",
            numberOfStrings: 6
        },
        {
            model: "Les Paul",
            brand: "Gibson",
            numberOfStrings: 6
        }
    ]

    return (
        <>{ myGuitars.map((guitar, index) => <Guitar key={index} guitar={guitar} />) }</>
    )
}

export default GuitarList