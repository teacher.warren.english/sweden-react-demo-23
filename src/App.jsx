import GuitarPage from "./pages/GuitarPage"
import CoffeePage from "./pages/CoffeePage"
import Navbar from "./components/Navbar"
import { BrowserRouter, Routes, Route, NavLink } from "react-router-dom"
import { useContext, useEffect, useState } from "react"
import { UserContext } from "./context/UserContext"

function App() {

  // const [username, setUsername] = useState("")
  const [username, setUsername] = useContext(UserContext)

  useEffect(() => {
    fetch("https://randomuser.me/api")
    .then(resp => resp.json())
    .then(json => {
      console.log(json.results[0].login.username);
      setUsername(json.results[0].login.username)
    })
    .catch(error => console.error(error))
  }, []) // runs once

  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<CoffeePage />} />
          <Route path="/guitars" element={<GuitarPage />} />
          <Route path="*" element={
            <>
              <h1>404 - Page Not Found</h1>
              <img src="https://i.pinimg.com/originals/41/1e/84/411e84f07501765e695c2e6d617a948b.gif" alt="ghost.gif" />
              <NavLink to='/' >Go back home</NavLink>
            </>
          } />
        </Routes>
      </BrowserRouter>
      {/* {username && <p>{ username }</p>} */}
    </>
  )
}

export default App