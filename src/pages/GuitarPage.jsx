import GuitarList from "../components/GuitarList"
import withAuth from "../hoc/withAuth"

function GuitarPage() {

    return (
        <GuitarList />
    )
}

export default withAuth(GuitarPage)