# React Demo Sweden 2023

This is a demo React application created using create-react-app.
We are exploring using React functional components, passing props to child components, using React hooks, managing local state within components and fetching data from an API to render.

## Installation

To clone this repository to your machine run the following command:

```bash
git clone https://gitlab.com/teacher.warren.english/sweden-react-demo-23.git
```

To install the required packages from NPM run the following command in the project root folder:

```bash
npm install
```

## Contributing

@teacher.warren.english - Noroff Accelerate AS

## License

[MIT](https://choosealicense.com/licenses/mit/)